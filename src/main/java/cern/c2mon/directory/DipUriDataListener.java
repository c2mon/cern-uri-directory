package cern.c2mon.directory;

import cern.dip.*;

public class DipUriDataListener implements DipSubscriptionListener {
    public void handleMessage(DipSubscription subscription, DipData message, DipSubscription sub[]){
        if (message.extractDataQuality() ==
                DipQuality.DIP_QUALITY_UNINITIALIZED){
            return;
        }
        if (subscription == sub[0]){
            try{
                System.out.println("New value for test.item1 is " +
                        message.extractInt());
            } catch(TypeMismatch e){
                System.out.println("test.item1 data is of unexpected type");
            }
        } else if (subscription == sub[1]){
            try{
                System.out.println("New values for test.item2 are "
                        + message.extractInt("field1") + " and "
                        + message.extractFloat("field2"));
            } catch(TypeMismatch e){
                System.out.println(
                        "test.item2 data is of unexpected type");
            } catch(BadParameter e){
                System.out.println("test.item2 has bad fields");
            }
        }
    }
    public void connected(DipSubscription arg0) {
        System.out.println("Publication source available");
    }

    @Override
    public void handleException(DipSubscription dipSubscription, Exception e) {

    }

    @Override
    public void handleMessage(DipSubscription dipSubscription, DipData dipData) {
        DipSubscriptionListener dsl = new DipUriDataListener();
        DipFactory df = Dip.create("uri-directory-harvester");
        DipBrowser browser = df.createDipBrowser();
        DipSubscription sub[] = new DipSubscription[2];
        for (int i=0; i < dipData.getTags().length; i++){
            // repository.save(DipTopic.builder().uri(dipSubscription.getTopicName()).label(dipData.getTags()[i]).domain("DIPCM").build());
        }
    }

    public void disconnected(DipSubscription arg0, String arg1) {
        System.out.println("Publication source unavailable");
    }
}