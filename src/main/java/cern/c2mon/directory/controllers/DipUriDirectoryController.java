package cern.c2mon.directory.controllers;

/**
 * Created by Eleni Mandilara on 28/9/2017.
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import cern.c2mon.directory.harvest.DipUriDirectory;
import cern.dip.*;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class DipUriDirectoryController {

    private static final List<String> directory = new ArrayList<>();

    @RequestMapping("/dip")
    public DipUriDirectory publicationsList() {
        directory.clear();
        DipFactory df = Dip.create("uri-directory-harvester");
        DipBrowser browser = df.createDipBrowser();
        directory.addAll(Arrays.asList(browser.getPublications("*")));
        return new DipUriDirectory(directory);
    }
}