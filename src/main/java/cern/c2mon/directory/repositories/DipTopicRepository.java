package cern.c2mon.directory.repositories;

import cern.c2mon.directory.model.DipTopic;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.web.bind.annotation.CrossOrigin;

/**
 * Created by Eleni Mandilara on 25/9/2017.
 */

@CrossOrigin
@EnableSpringDataWebSupport
public interface DipTopicRepository extends PagingAndSortingRepository<DipTopic, String>, QueryDslPredicateExecutor<DipTopic> {}