package cern.c2mon.directory;

import cern.c2mon.directory.harvest.DipHarvester;
import cern.c2mon.directory.repositories.DipTopicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * Created by Eleni Mandilara on 25/9/2017.
 */
@Component
public class DataInitializer implements ApplicationRunner {
    @Autowired
    DipTopicRepository dipTopicRepository;

    @Autowired
    DipHarvester dipHarvester;

    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
        dipHarvester.harvest();
    }
}
