package cern.c2mon.directory.harvest;

import cern.c2mon.directory.*;
import cern.c2mon.directory.model.DipTopic;
import cern.c2mon.directory.repositories.DipTopicRepository;
import cern.dip.*;
import cern.dip.DipSubscriptionListener;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Eleni Mandilara on 9/26/2017.
 */
@Component
@Log
public class DipHarvester {

    @Autowired
    DipTopicRepository repository;

    public void harvest() throws DipException {
        DipSubscriptionListener dsl = new DipUriDataListener();
        DipFactory df = Dip.create("uri-directory-harvester");
        DipBrowser browser = df.createDipBrowser();

        for(String pub : browser.getPublications("*")){
            DipTopic dipTopic = DipTopic.builder().topic(pub).publisher("TEST publisher").host("TEST host").uri("dip://"+pub).build();
            repository.save(dipTopic);
        }
    }
};
