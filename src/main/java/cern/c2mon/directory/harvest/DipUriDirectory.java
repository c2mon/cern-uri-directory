package cern.c2mon.directory.harvest;

import java.util.List;

/**
 * Created by Eleni Mandilara on 28/9/2017
 */
public class DipUriDirectory {
    private final List<String> dipUriDirectory;

    public DipUriDirectory(List<String> dipUriDirectory) {
        this.dipUriDirectory = dipUriDirectory;
    }
    public List<String> getDipUriDirectory() {
        return dipUriDirectory;
    }
}