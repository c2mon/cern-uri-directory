package cern.c2mon.directory.model;

import com.querydsl.core.annotations.QueryEntity;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import java.util.UUID;

/**
 * Created by Eleni Mandilara on 25/9/2017.
 */

@Data
@QueryEntity
@Builder
public class DipTopic {
    @Id
    @Builder.Default
    String id = UUID.randomUUID().toString();

    String uri;
    String topic;
    String publisher;
    String host;
    String type;
}