CERN URI DIRECTORY
--------------------
A simple HATEOAS REST API to advertise data sources programmatically.


More Dependencies
--------------------

The jar `dip-jni-5.6.3.jar` from `dip-5.6.3-distribution/dip-5.6.3/lib64/` needs to be added manually to the project.


Install and Run
--------------------
Comment out the `com.spotify:docker-maven-plugin` from the `pom.xml` and then, to install the project, do

`mvn clean install -DskipTests`

To run, add the the directory that hosts the DIP-related `.dll` files (`jdim.dll`, `jDIP.dll`, `log4cplus.dll`) to your working directory options,
and then run the Spring Boot application:


`mvn spring-boot:run`

Browse DIP directory
-----------
To view the directory entries for DIP, for detailed information go to
`http://localhost:8080/rest/dipTopics`, or to `http://localhost:8080/dip`
that contains only the names of the DIP topics.

